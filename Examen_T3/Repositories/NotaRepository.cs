﻿using Examen_T3.BDD;
using Examen_T3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3.Repositories
{

    public interface INotaRepository
    {
        List<Nota> GetAll(bool? isDeleted = null);
        bool Create(Nota nota);
        bool Delete(Nota nota);
        bool Update(Nota nota);
    }
    public class NotaRepository: INotaRepository
    {
        private readonly NotaContext context;

        public NotaRepository(NotaContext context)
        {
            this.context = context;
        }

        public bool Create(Nota nota)
        {
            if(nota != null)
            {
                context.notas.Add(nota);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Delete(Nota nota)
        {
            if (nota != null)
            {
                context.notas.Remove(nota);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool Update(Nota nota)
        {
            if (nota != null)
            {
                context.notas.Update(nota);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Nota> GetAll(bool? isDeleted = null)
        {
            var no = context.notas;
            var nota = no.ToList();
            return nota;
        }

    }
}
