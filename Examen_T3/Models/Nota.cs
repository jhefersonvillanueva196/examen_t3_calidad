﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3.Models
{
    public class Nota
    {
        public int Id_nota { get; set; }
        public string Titulo { get; set; }
        public string Contenido { get; set; }
        public string Fecha { get; set; }
    }
}
