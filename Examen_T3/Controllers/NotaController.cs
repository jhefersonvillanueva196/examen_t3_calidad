﻿using Examen_T3.BDD;
using Examen_T3.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3.Controllers
{
    public class NotaController : Controller
    {
        private readonly NotaContext context;

        public NotaController(NotaContext context)
        {
            this.context = context;
        }

        public List<Nota> getNotas()
        {
            var no = context.notas;
            var nota = no.ToList();
            return nota;
        }

        public Nota buscaNota(int id)
        {
            var no = context.notas;
            Nota nota = no.FirstOrDefault(no =>no.Id_nota == id);
            return nota;
        }

        [HttpGet]
        public IActionResult MostrarNotas()
        {
            var liNotas = getNotas();
            return View("Notas", liNotas);
        }


        [HttpGet]
        public IActionResult NotaNueva()
        {
            return View();
        }
        [HttpPost]
        public IActionResult NotaNueva(string titulo, string contenido, string fecha)
        {
            Nota no = new Nota
            {
                Titulo = titulo,
                Contenido = contenido,
                Fecha = fecha
            };
            context.notas.Add(no);
            context.SaveChanges();
            var liNotas = getNotas();
            return View("Notas", liNotas);
        }
        [HttpGet]
        public IActionResult Nota(int id)
        {
            var nota = buscaNota(id);
            return View("Nota",nota);
        }
        [HttpGet]
        public IActionResult EditarNota(int id)
        {
            var nota = buscaNota(id);
            return View("EditarNota",nota);
        }
        [HttpPost]
        public IActionResult EditarNota(int Id,string Titulo, string Contenido, string Fecha)
        {
            var liNotas = getNotas();
            var nota = buscaNota(Id);
            nota.Titulo = Titulo;
            nota.Contenido = Contenido;
            nota.Fecha = Fecha;
            context.notas.Update(nota);
            context.SaveChanges();
            return View("Notas", liNotas);
        }
        [HttpPost]
        public IActionResult EliminarNota(int id)
        {
            var liNotas = getNotas();
            var nota = buscaNota(id);
            context.notas.Remove(nota);
            context.SaveChanges();
            return View("Notas", liNotas);
        }
    }
}
