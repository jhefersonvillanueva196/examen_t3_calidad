﻿using Examen_T3.BDD.Mapping;
using Examen_T3.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3.BDD
{
    public class NotaContext:DbContext
    {
        public DbSet<Nota> notas { get; set; }

        public NotaContext(DbContextOptions<NotaContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new NotaMap());
        }
    }
}
