﻿using Examen_T3.BDD;
using Examen_T3.Models;
using Examen_T3.Repositories;
using Examen_T3.Test.Repositories.Mocks;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_T3.Test.Repositories
{
    public class NotaRepositoryTest
    {
        private Mock<NotaContext> mockContext;

        [SetUp]
        public void SetUp()
        {
            mockContext = NotaContextMock.GetApplicationContextMock();
        }

        [Test]
        public void TestGetAll()
        {
            var respository = new NotaRepository(mockContext.Object);
            var notas = respository.GetAll();

            Assert.AreEqual(4, notas.Count);
        }

        [Test]
        public void TestCreate()
        {
            var respository = new NotaRepository(mockContext.Object);
            Nota nota = new Nota()
            {
                Titulo = "Prueba 1",
                Contenido = "Estoy probando la creacion de notas",
                Fecha = "06/11/2021"
            };
            var notas = respository.Create(nota);

            Assert.AreEqual(true, notas);
        }

        public void TestCreateFalse()
        {
            var respository = new NotaRepository(mockContext.Object);
            Nota nota = new Nota();
            var notas = respository.Create(nota);

            Assert.AreEqual(false, notas);
        }

        [Test]
        public void TestDelete()
        {
            var respository = new NotaRepository(mockContext.Object);
            Nota nota = new Nota()
            {
                Titulo = "Prueba 1",
                Contenido = "Estoy probando la creacion de notas",
                Fecha = "06/11/2021"
            };
            var notas = respository.Create(nota);

            Assert.AreEqual(true, notas);
        }

        [Test]
        public void TestDeleteFalse()
        {
            var respository = new NotaRepository(mockContext.Object);
            Nota nota = new Nota();
            var notas = respository.Create(nota);

            Assert.AreEqual(false, notas);
        }

        [Test]
        public void TestUpdate()
        {
            var respository = new NotaRepository(mockContext.Object);
            Nota nota = new Nota()
            {
                Titulo = "Prueba 1",
                Contenido = "Estoy probando la creacion de notas",
                Fecha = "06/11/2021"
            };
            var notas = respository.Create(nota);

            Assert.AreEqual(true, notas);
        }

        [Test]
        public void TestUpdateFalse()
        {
            var respository = new NotaRepository(mockContext.Object);
            Nota nota = new Nota();
            var notas = respository.Create(nota);

            Assert.AreEqual(false, notas);
        }
    }
}
