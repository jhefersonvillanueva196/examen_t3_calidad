﻿using Examen_T3.BDD;
using Examen_T3.Models;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Examen_T3.Test.Repositories.Mocks
{
    public static class NotaContextMock
    {
        public static Mock<NotaContext> GetApplicationContextMock()
        {
            IQueryable<Nota> notaData = GetNotaData();

            var mockDbSetUser = new Mock<DbSet<Nota>>();
            mockDbSetUser.As<IQueryable<Nota>>().Setup(m => m.Provider).Returns(notaData.Provider);
            mockDbSetUser.As<IQueryable<Nota>>().Setup(m => m.Expression).Returns(notaData.Expression);
            mockDbSetUser.As<IQueryable<Nota>>().Setup(m => m.ElementType).Returns(notaData.ElementType);
            mockDbSetUser.As<IQueryable<Nota>>().Setup(m => m.GetEnumerator()).Returns(notaData.GetEnumerator());
            mockDbSetUser.Setup(m => m.AsQueryable()).Returns(notaData);

            var mockContext = new Mock<NotaContext>(new DbContextOptions<NotaContext>());
            mockContext.Setup(c => c.notas).Returns(mockDbSetUser.Object);

            return mockContext;
        }

        private static IQueryable<Nota> GetNotaData()
        {
            return new List<Nota>
            {
                new Nota { Id_nota = 1, Titulo = "admin", Contenido = "admin", Fecha = "06/11/2021"},
                new Nota { Id_nota = 2, Titulo = "user1", Contenido = "user1", Fecha = "08/11/2021"},
                new Nota { Id_nota = 3, Titulo = "user2", Contenido = "user2", Fecha = "10/11/2021"},
                new Nota { Id_nota = 4, Titulo = "user3", Contenido = "user3", Fecha = "20/11/2021"},
            }.AsQueryable();
        }
    }
}
